(ns gig.moderate)

;; Flipping outSolutions
;; Difficulty:	Medium
;; Topics:	higher-order-functions

;; Write a higher-order function which flips the order of the arguments of an input function.
;; test not run

(= 3 ((__ nth) 2 [1 2 3 4 5]))

(= true ((__ >) 7 8))

(= 4 ((__ quot) 2 8))

(= [1 2 3] ((__ take) [1 2 3 4 5] 3))

(fn [f] (fn [x y] (f y x)))


;; Rotate SequenceSolutions
;; Difficulty:	Medium
;; Topics:	seqs


;; Write a function which can rotate a sequence in either direction.

(= (__ 2 [1 2 3 4 5]) '(3 4 5 1 2))

(= (__ -2 [1 2 3 4 5]) '(4 5 1 2 3))

(= (__ 6 [1 2 3 4 5]) '(2 3 4 5 1))

(= (__ 1 '(:a :b :c)) '(:b :c :a))

(= (__ -4 '(:a :b :c)) '(:c :a :b))

(fn [n coll]
    (let [c (count coll)
          t (Math/abs n)
          i (if (> t c) (- t c) t)
          f (fn [i coll] (->> (split-at i coll) (reverse) (flatten)))]
      (if (pos? n)
        (f i coll)
        (f (- c i) coll))))

;; Reverse InterleaveSolutions
;; Difficulty:	Medium
;; Topics:	seqs


;; Write a function which reverses the interleave process into x number of subsequences.

(= (__ [1 2 3 4 5 6] 2) '((1 3 5) (2 4 6)))

(= (__ (range 9) 3) '((0 3 6) (1 4 7) (2 5 8)))

(= (__ (range 10) 5) '((0 5) (1 6) (2 7) (3 8) (4 9)))

(fn [coll n] (apply map list (partition n coll)))


;; Split by TypeSolutions
;; Difficulty:	Medium
;; Topics:	seqs


;; Write a function which takes a sequence consisting of items with different types and splits them up into a set of homogeneous sub-sequences. The internal order of each sub-sequence should be maintained, but the sub-sequences themselves can be returned in any order (this is why 'set' is used in the test cases).

(= (set (__ [1 :a 2 :b 3 :c])) #{[1 2 3] [:a :b :c]})

(= (set (__ [:a "foo"  "bar" :b])) #{[:a :b] ["foo" "bar"]})

(= (set (__ [[1 2] :a [3 4] 5 6 :b])) #{[[1 2] [3 4]] [:a :b] [5 6]})

#(vals (group-by type %))


;; Count OccurrencesSolutions
;; Difficulty:	Medium
;; Topics:	seqs core-functions


;; Write a function which returns a map containing the number of occurences of each distinct item in a sequence.

(= (__ [1 1 2 3 2 1 1]) {1 4, 2 2, 3 1})

(= (__ [:b :a :b :a :b]) {:a 2, :b 3})

(= (__ '([1 2] [1 3] [1 3])) {[1 2] 1, [1 3] 2})

;; Special Restrictions
;; frequencies

(fn [coll] (->> (group-by identity coll)
               (reduce-kv (fn [r k v] (assoc r k (count v))) {})))

;; Find Distinct ItemsSolutions
;; Difficulty:	Medium
;; Topics:	seqs core-functions


;; Write a function which removes the duplicates from a sequence. Order of the items must be maintained.

(= (__ [1 2 1 3 1 2 4]) [1 2 3 4])

(= (__ [:a :a :b :b :c :c]) [:a :b :c])

(= (__ '([2 4] [1 2] [1 3] [1 3])) '([2 4] [1 2] [1 3]))

(= (__ (range 50)) (range 50))

;; Special Restrictions
;; distinct

reduce (fn [r v] (if (some #{v} r) r (conj r v))) []


;; Function CompositionSolutions
;; Difficulty:	Medium
;; Topics:	higher-order-functions core-functions


;; Write a function which allows you to create function compositions. The parameter list should take a variable number of functions, and create a function that applies them from right-to-left.

(= [3 2 1] ((__ rest reverse) [1 2 3 4]))

(= 5 ((__ (partial + 3) second) [1 2 3 4]))

(= true ((__ zero? #(mod % 8) +) 3 5 7 9))

(= "HELLO" ((__ #(.toUpperCase %) #(apply str %) take) 5 "hello world"))

(fn [f g & fs]
    (fn [x & xs]
      (if (seq fs)
        (f (g (apply (last fs) x xs)))
        (f (g x)))))


;; Partition a SequenceSolutions
;; Difficulty:	Medium
;; Topics:	seqs core-functions


;; Write a function which returns a sequence of lists of x items each. Lists of less than x items should not be returned.

(= (__ 3 (range 9)) '((0 1 2) (3 4 5) (6 7 8)))

(= (__ 2 (range 8)) '((0 1) (2 3) (4 5) (6 7)))

(= (__ 3 (range 8)) '((0 1 2) (3 4 5)))

;; Special Restrictions
;; partition
;; partition-all

(fn [n coll]
    (loop [r [] c coll]
      (if (seq c)
        (recur (conj r (take n c)) (drop n c))
        (filter #(= (count %) n) r))))


;; JuxtapositionSolutions
;; Difficulty:	Medium
;; Topics:	higher-order-functions core-functions


;; Take a set of functions and return a new function that takes a variable number of arguments and returns a sequence containing the result of applying each function left-to-right to the argument list.

(= [21 6 1] ((__ + max min) 2 3 5 1 6 4))

(= ["HELLO" 5] ((__ #(.toUpperCase %) count) "hello"))

(= [2 6 4] ((__ :a :c :b) {:a 2, :b 4, :c 6, :d 8 :e 10}))

;; Special Restrictions
;; juxt

(fn [& fs]
    (fn [& xs]
      (map #(apply % xs) fs)))


;; Word SortingSolutions
;; Difficulty:	Medium
;; Topics:	sorting


;; Write a function that splits a sentence up into a sorted list of words. Capitalization should not affect sort order and punctuation should be ignored.

(= (__  "Have a nice day.")
   ["a" "day" "Have" "nice"])

(= (__  "Clojure is a fun language!")
   ["a" "Clojure" "fun" "is" "language"])

(= (__  "Fools fall for foolish follies.")
   ["fall" "follies" "foolish" "Fools" "for"])


(fn [s] (sort-by clojure.string/lower-case (re-seq #"\w+" s)))


;; Prime NumbersSolutions
;; Difficulty:	Medium
;; Topics:	primes


;; Write a function which returns the first x number of prime numbers.

(= (__ 2) [2 3])

(= (__ 5) [2 3 5 7 11])

(= (last (__ 100)) 541)


(fn [n]
    (let [prime? (fn [n]
                   (if (< n 2)
                     false
                     (not (some zero? (map (fn [v] (rem n v)) (range 2 n))))))]
      (take n (filter prime? (range)))))


;; Black Box TestingSolutions
;; Difficulty:	Medium
;; Topics:	seqs testing


;; Clojure has many sequence types, which act in subtly different ways. The core functions typically convert them into a uniform "sequence" type and work with them that way, but it can be important to understand the behavioral and performance differences so that you know which kind is appropriate for your application.

;; Write a function which takes a collection and returns one of :map, :set, :list, or :vector - describing the type of collection it was given.
;; You won't be allowed to inspect their class or use the built-in predicates like list? - the point is to poke at them and understand their behavior.

(= :map (__ {:a 1, :b 2}))

(= :list (__ (range (rand-int 20))))

(= :vector (__ [1 2 3 4 5 6]))

(= :set (__ #{10 (rand-int 5)}))

(= [:map :set :vector :list] (map __ [{} #{} [] ()]))

;; Special Restrictions
;; class
;; type
;; Class
;; vector?
;; sequential?
;; list?
;; seq?
;; map?
;; set?
;; instance?
;; getClass

(fn [coll]
    (let [[s] (str coll)]
      (cond
        (= s \{) :map
        (= s \[) :vector
        (= s \#) :set
        :default :list)))


;; Filter Perfect SquaresSolutions
;; Difficulty:	Medium
;; Topics:	


;; Given a string of comma separated integers, write a function which returns a new comma separated string that only contains the numbers which are perfect squares.

(= (__ "4,5,6,7,8,9") "4,9")

(= (__ "15,16,25,36,37") "16,25,36")


(fn [coll]
    (->> (clojure.string/split coll #",")
         (filter #(let [s (Math/sqrt (read-string %))] (== (int s) s)))
         (clojure.string/join "," )))


;; Intro to TrampolineSolutions
;; Difficulty:	Medium
;; Topics:	recursion


;; The trampoline function takes a function f and a variable number of parameters. Trampoline calls f with any parameters that were supplied. If f returns a function, trampoline calls that function with no arguments. This is repeated, until the return value is not a function, and then trampoline returns that non-function value. This is useful for implementing mutually recursive algorithms in a way that won't consume the stack.

(= __
   (letfn
     [(foo [x y] #(bar (conj x y) y))
      (bar [x y] (if (> (last x) 10)
                   x
                   #(foo x (+ 2 y))))]
     (trampoline foo [] 1)))


;; Anagram FinderSolutions
;; Difficulty:	Medium
;; Topics:	


;; Write a function which finds all the anagrams in a vector of words. A word x is an anagram of word y if all the letters in x can be rearranged in a different order to form y. Your function should return a set of sets, where each sub-set is a group of words which are anagrams of each other. Each sub-set should have at least two words. Words without any anagrams should not be included in the result.

(= (__ ["meat" "mat" "team" "mate" "eat"])
   #{#{"meat" "team" "mate"}})

(= (__ ["veer" "lake" "item" "kale" "mite" "ever"])
   #{#{"veer" "ever"} #{"lake" "kale"} #{"mite" "item"}})

(fn [coll]
    (->> (reduce (fn [r v] (conj r (set (filter #(= (sort (seq v)) (sort (seq %))) coll)))) #{} coll)
         (filter #(< 1 (count %)))
         (set)))


;; Sequence ReductionsSolutions
;; Difficulty:	Medium
;; Topics:	seqs core-functions


;; Write a function which behaves like reduce, but returns each intermediate value of the reduction. Your function must accept either two or three arguments, and the return sequence must be lazy.

(= (take 5 (__ + (range))) [0 1 3 6 10])

(= (__ conj [1] [2 3 4]) [[1] [1 2] [1 2 3] [1 2 3 4]])

(= (last (__ * 2 [3 4 5])) (reduce * 2 [3 4 5]) 120)

;; Special Restrictions
;; reductions

(fn r
  ([f [x & xs]] (r f x xs))
  ([f i [x & xs]] (if x (lazy-seq (cons i (r f (f i x) xs))) [i])))

;; Perfect NumbersSolutions
;; Difficulty:	Medium
;; Topics:	


;; A number is "perfect" if the sum of its divisors equal the number itself. 6 is a perfect number because 1+2+3=6. Write a function which returns true for perfect numbers and false otherwise.

(= (__ 6) true)

(= (__ 7) false)

(= (__ 496) true)

(= (__ 500) false)

(= (__ 8128) true)


(fn [n] (= n (reduce + (filter #(zero? (mod n %)) (range 1 n)))))


;; Merge with a FunctionSolutions
;; Difficulty:	Medium
;; Topics:	core-functions


;; Write a function which takes a function f and a variable number of maps. Your function should return a map that consists of the rest of the maps conj-ed onto the first. If a key occurs in more than one map, the mapping(s) from the latter (left-to-right) should be combined with the mapping in the result by calling (f val-in-result val-in-latter)

(= (__ * {:a 2, :b 3, :c 4} {:a 2} {:b 2} {:c 5})
   {:a 4, :b 6, :c 20})

(= (__ - {1 10, 2 20} {1 3, 2 10, 3 15})
   {1 7, 2 10, 3 15})

(= (__ concat {:a [3], :b [6]} {:a [4 5], :c [8 9]} {:b [7]})
   {:a [3 4 5], :b [6 7], :c [8 9]})

;; Special Restrictions
;; merge-with

(fn [f coll & args]
  (reduce-kv (fn [r k v]
               (if (r k)
                 (assoc r k (apply f (r k) [v]))
                 (assoc r k v)))
             coll (into {} args)))


;; intoCamelCaseSolutions
;; Difficulty:	Medium
;; Topics:	strings


;; When working with java, you often need to create an object with fieldsLikeThis, but you'd rather work with a hashmap that has :keys-like-this until it's time to convert. Write a function which takes lower-case hyphen-separated strings and converts them to camel-case strings.

(= (__ "something") "something")

(= (__ "multi-word-key") "multiWordKey")

(= (__ "leaveMeAlone") "leaveMeAlone")

#(let [[x & xs] (clojure.string/split % #"-")]
   (clojure.string/join (cons x (map clojure.string/capitalize xs))))


;; Euler's Totient FunctionSolutions
;; Difficulty:	Medium
;; Topics:	


;; Two numbers are coprime if their greatest common divisor equals 1. Euler's totient function f(x) is defined as the number of positive integers less than x which are coprime to x. The special case f(1) equals 1. Write a function which calculates Euler's totient function.

(= (__ 1) 1)

(= (__ 10) (count '(1 3 7 9)) 4)

(= (__ 40) 16)

(= (__ 99) 60)

(fn [n]
    (if (= n 1) 1
      (let [gcd (fn [x y] (if (zero? y) x (recur y (rem x y))))]
        (count (filter (fn [v] (= (gcd n v) 1)) (range 1 n))))))


;; Reimplement TrampolineSolutions
;; Difficulty:	Medium
;; Topics:	core-functions


;; Reimplement the function described in "Intro to Trampoline".

(= (letfn [(triple [x] #(sub-two (* 3 x)))
          (sub-two [x] #(stop?(- x 2)))
          (stop? [x] (if (> x 50) x #(triple x)))]
    (__ triple 2))
  82)

(= (letfn [(my-even? [x] (if (zero? x) true #(my-odd? (dec x))))
          (my-odd? [x] (if (zero? x) false #(my-even? (dec x))))]
    (map (partial __ my-even?) (range 6)))
  [true false true false true false])

;; Special Restrictions
;; trampoline

(fn [f & args]
    (loop [r (apply f args)]
      (if (fn? r) (recur (r)) r)))


;; Longest Increasing Sub-SeqSolutions
;; Difficulty:	Hard
;; Topics:	seqs


;; Given a vector of integers, find the longest consecutive sub-sequence of increasing numbers. If two sub-sequences have the same length, use the one that occurs first. An increasing sub-sequence must have a length of 2 or greater to qualify.

(= (__ [1 0 1 2 3 0 4 5]) [0 1 2 3])

(= (__ [5 6 1 3 2 7]) [5 6])

(= (__ [2 3 3 4 5]) [3 4 5])

(= (__ [7 6 5 4]) [])


(fn f [coll]
    (let [dedup (fn [coll] (reduce (fn [r v] (if (= (last r) v) r (conj r v))) [] coll))
          raw (loop [[x y :as c] coll r []]
                (if (seq c)
                  (if (= x ((fnil dec x) y))
                    (recur (rest c) (conj r x y))
                    (recur (rest c) (conj r nil)))
                  (dedup r)))
          parted (partition-by type raw)
          data (reduce (fn [r v] (if (number? (first v))
                                   (assoc r (count v) v)
                                   r))
                       {} parted)
          r (if (seq data) (get data (apply max (keys data))) [])]
      (if (> 1 (count r))
        []
        r)))
