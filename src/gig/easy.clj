(ns gig.easy
  (:require [clojure.test :refer :all]))

;; Problem
;; Solution



;; Last ElementSolutions
;; Difficulty:	Easy
;; Topics:	seqs core-functions


;; Write a function which returns the last element in a sequence.
;; 
;; (= (__ [1 2 3 4 5]) 5)
;; 
;; (= (__ '(5 4 3)) 3)
;; 
;; (= (__ ["b" "c" "d"]) "d")

;; solution
(comp first reverse)


;; Penultimate ElementSolutions
;; Difficulty:	Easy
;; Topics:	seqs


;; Write a function which returns the second to last element from a sequence.
;; 
;; (= (__ (list 1 2 3 4 5)) 4)
;; 
;; (= (__ ["a" "b" "c"]) "b")
;; 
;; (= (__ [[1 2] [3 4]]) [1 2])

(comp second reverse)


;; 156 Map DefaultsSolutions
;; Difficulty:	Elementary
;; Topics:	seqs


;; When retrieving values from a map, you can specify default values in case the key is not found:

;; (= 2 (:foo {:bar 0, :baz 1} 2))

;; However, what if you want the map itself to contain the default values? Write a function which takes a default value and a sequence of keys and constructs a map.
;; 
;; (= (__ 0 [:a :b :c]) {:a 0 :b 0 :c 0})
;; 
;; (= (__ "x" [1 2 3]) {1 "x" 2 "x" 3 "x"})
;; 
;; (= (__ [:a :b] [:foo :bar]) {:foo [:a :b] :bar [:a :b]})

(fn [x xs] (zipmap xs (repeat x)))


;; Nth ElementSolutions
;; Difficulty:	Easy
;; Topics:	seqs core-functions


;; Write a function which returns the Nth element from a sequence.
;; 
;; (= (__ '(4 5 6 7) 2) 6)
;; 
;; (= (__ [:a :b :c] 0) :a)
;; 
;; (= (__ [1 2 3 4] 1) 2)
;; 
;; (= (__ '([1 2] [3 4] [5 6]) 2) [5 6])

(fn [coll n]
	(first (drop n coll)))


;; Count a SequenceSolutions
;; Difficulty:	Easy
;; Topics:	seqs core-functions


;; Write a function which returns the total number of elements in a sequence.
;; 
;; (= (__ '(1 2 3 3 1)) 5)
;; 
;; (= (__ "Hello World") 11)
;; 
;; (= (__ [[1 2] [3 4] [5 6]]) 3)
;; 
;; (= (__ '(13)) 1)
;; 
;; (= (__ '(:a :b :c)) 3)

#(reduce (fn [r v] (inc r)) 0 %)


;; Sum It All UpSolutions
;; Difficulty:	Easy
;; Topics:	seqs


;; Write a function which returns the sum of a sequence of numbers.
;; 
;; (= (__ [1 2 3]) 6)
;; 
;; (= (__ (list 0 -2 5 5)) 8)
;; 
;; (= (__ #{4 2 1}) 7)
;; 
;; (= (__ '(0 0 -1)) -1)
;; 
;; (= (__ '(1 10 3)) 14)

reduce +


;; 25Find the odd numbersSolutions
;; Difficulty:	Easy
;; Topics:	seqs


;; Write a function which returns only the odd numbers from a sequence.
;; 
;; (= (__ #{1 2 3 4 5}) '(1 3 5))
;; 
;; (= (__ [4 2 1 6]) '(1))
;; 
;; (= (__ [2 2 4 6]) '())
;; 
;; (= (__ [1 1 1 3]) '(1 1 1 3))

filter odd?


;; 23Reverse a SequenceSolutions
;; Difficulty:	Easy
;; Topics:	seqs core-functions


;; Write a function which reverses a sequence.
;; 
;; (= (__ [1 2 3 4 5]) [5 4 3 2 1])
;; 
;; (= (__ (sorted-set 5 7 2 7)) '(7 5 2))
;; 
;; (= (__ [[1 2][3 4][5 6]]) [[5 6][3 4][1 2]])

into '()


;; Palindrome DetectorSolutions
;; Difficulty:	Easy
;; Topics:	seqs


;; Write a function which returns true if the given sequence is a palindrome.

;; Hint: "racecar" does not equal '(\r \a \c \e \c \a \r)

;; (false? (__ '(1 2 3 4 5)))

;; (true? (__ "racecar"))

;; (true? (__ [:foo :bar :foo]))

;; (true? (__ '(1 1 3 3 1 1)))

;; (false? (__ '(:a :b :c)))


(fn [coll] (= (seq coll) (reverse coll)))


;; Fibonacci SequenceSolutions
;; Difficulty:	Easy
;; Topics:	Fibonacci seqs


;; Write a function which returns the first X fibonacci numbers.

(= (__ 3) '(1 1 2))

(= (__ 6) '(1 1 2 3 5 8))

(= (__ 8) '(1 1 2 3 5 8 13 21))

#(reverse (reduce (fn [[x y :as r] n] (cons (+' x y) r)) [1 1] (range 2 %)))


;; Maximum valueSolutions
;; Difficulty:	Easy
;; Topics:	core-functions


;; Write a function which takes a variable number of parameters and returns the maximum value.

(= (__ 1 8 3 4) 8)

(= (__ 30 20) 30)

(= (__ 45 67 11) 67)


(fn [& args] (last (sort args)))


;; Get the CapsSolutions
;; Difficulty:	Easy
;; Topics:	strings


;; Write a function which takes a string and returns a new string containing only the capital letters.

(= (__ "HeLlO, WoRlD!") "HLOWRD")

(empty? (__ "nothing"))

(= (__ "$#A(*&987Zf") "AZ")


#(clojure.string/join (re-seq #"[A-Z]" %))


;; Duplicate a SequenceSolutions
;; Difficulty:	Easy
;; Topics:	seqs


;; Write a function which duplicates each element of a sequence.

(= (__ [1 2 3]) '(1 1 2 2 3 3))

(= (__ [:a :a :b :b]) '(:a :a :a :a :b :b :b :b))

(= (__ [[1 2] [3 4]]) '([1 2] [1 2] [3 4] [3 4]))


mapcat #(list % %)


;; Compress a SequenceSolutions
;; Difficulty:	Easy
;; Topics:	seqs


;; Write a function which removes consecutive duplicates from a sequence.

(= (apply str (__ "Leeeeeerrroyyy")) "Leroy")

(= (__ [1 1 2 3 3 2 2 3]) '(1 2 3 2 3))

(= (__ [[1 2] [1 2] [3 4] [1 2]]) '([1 2] [3 4] [1 2]))


(fn [coll] (map first (partition-by identity coll)))


;; Implement rangeSolutions
;; Difficulty:	Easy
;; Topics:	seqs core-functions


;; Write a function which creates a list of all integers in a given range.

(= (__ 1 4) '(1 2 3))

(= (__ -2 2) '(-2 -1 0 1))

(= (__ 5 8) '(5 6 7))

(fn f [a b] (when (< a b) (cons a (f (inc a) b))))


;; 42Factorial FunSolutions
;; Difficulty:	Easy
;; Topics:	math


;; Write a function which calculates factorials.

(= (__ 1) 1)

(= (__ 3) 6)

(= (__ 5) 120)

(= (__ 8) 40320)


(fn [n] (reduce * (range 1 (inc n))))


;; Interleave Two SeqsSolutions
;; Difficulty:	Easy
;; Topics:	seqs core-functions


;; Write a function which takes two sequences and returns the first item from each, then the second item from each, then the third, etc.

(= (__ [1 2 3] [:a :b :c]) '(1 :a 2 :b 3 :c))

(= (__ [1 2] [3 4 5 6]) '(1 3 2 4))

(= (__ [1 2 3 4] [5]) [1 5])

(= (__ [30 20] [25 15]) [30 25 20 15])


mapcat list


;; Flatten a SequenceSolutions
;; Difficulty:	Easy
;; Topics:	seqs core-functions


;; Write a function which flattens a sequence.

(= (__ '((1 2) 3 [4 [5 6]])) '(1 2 3 4 5 6))

(= (__ ["a" ["b"] "c"]) '("a" "b" "c"))

(= (__ '((((:a))))) '(:a))


(fn flat [coll]
   (if (coll? coll)
     (mapcat flat coll)
     [coll]))


;; Replicate a SequenceSolutions
;; Difficulty:	Easy
;; Topics:	seqs


;; Write a function which replicates each element of a sequence a variable number of times.

(= (__ [1 2 3] 2) '(1 1 2 2 3 3))

(= (__ [:a :b] 4) '(:a :a :a :a :b :b :b :b))

(= (__ [4 5 6] 1) '(4 5 6))

(= (__ [[1 2] [3 4]] 2) '([1 2] [1 2] [3 4] [3 4]))

(= (__ [44 33] 2) [44 44 33 33])


(fn [coll n] (mapcat #(repeat n %) coll))

;; 45Intro to IterateSolutions
;; Difficulty:	Easy
;; Topics:	seqs


;; The iterate function can be used to produce an infinite lazy sequence.

(= __ (take 5 (iterate #(+ 3 %) 1)))

'(1 4 7 10 13)


;; Contain YourselfSolutions
;; Difficulty:	Easy
;; Topics:	


;; The contains? function checks if a KEY is present in a given collection. This often leads beginner clojurians to use it incorrectly with numerically indexed collections like vectors and lists.

(contains? #{4 5 6} __)

(contains? [1 1 1 1 1] __)

(contains? {4 :a 2 :b} __)

(not (contains? [1 2 4] __))

4


;; 31Pack a SequenceSolutions
;; Difficulty:	Easy
;; Topics:	seqs


;; Write a function which packs consecutive duplicates into sub-lists.

(= (__ [1 1 2 1 1 1 3 3]) '((1 1) (2) (1 1 1) (3 3)))

(= (__ [:a :a :b :b :c]) '((:a :a) (:b :b) (:c)))

(= (__ [[1 2] [1 2] [3 4]]) '(([1 2] [1 2]) ([3 4])))

(fn [coll] (partition-by identity coll))


;; Interpose a SeqSolutions
;; Difficulty:	Easy
;; Topics:	seqs core-functions


;; Write a function which separates the items of a sequence by an arbitrary value.

(= (__ 0 [1 2 3]) [1 0 2 0 3])

(= (apply str (__ ", " ["one" "two" "three"])) "one, two, three")

(= (__ :z [:a :b :c :d]) [:a :z :b :z :c :z :d])

;; Special Restrictions
;; interpose

(fn [n coll] (rest (mapcat #(list n %) coll)))


;; rop Every Nth ItemSolutions
;; Difficulty:	Easy
;; Topics:	seqs


;; Write a function which drops every Nth item from a sequence.

(= (__ [1 2 3 4 5 6 7 8] 3) [1 2 4 5 7 8])

(= (__ [:a :b :c :d :e :f] 2) [:a :c :e])

(= (__ [1 2 3 4 5 6] 4) [1 2 3 5 6])


(fn [coll n] (flatten (partition-all (dec n) n coll)))

;; Split a sequenceSolutions
;; Difficulty:	Easy
;; Topics:	seqs core-functions


;; Write a function which will split a sequence into two parts.

(= (__ 3 [1 2 3 4 5 6]) [[1 2 3] [4 5 6]])

(= (__ 1 [:a :b :c :d]) [[:a] [:b :c :d]])

(= (__ 2 [[1 2] [3 4] [5 6]]) [[[1 2] [3 4]] [[5 6]]])

(juxt take drop)

;; Advanced DestructuringSolutions
;; Difficulty:	Easy
;; Topics:	destructuring


;; Here is an example of some more sophisticated destructuring.

(= [1 2 [3 4 5] [1 2 3 4 5]] (let [[a b & c :as d] __] [a b c d]))


[1 2 3 4 5]


;; A Half-TruthSolutions
;; Difficulty:	Easy
;; Topics:	


;; Write a function which takes a variable number of booleans. Your function should return true if some of the parameters are true, but not all of the parameters are true. Otherwise your function should return false.

(= false (__ false false))

(= true (__ true false))

(= false (__ true))

(= true (__ false true false))

(= false (__ true true true))

(= true (__ true true true false))

not=

;; Map ConstructionSolutions
;; Difficulty:	Easy
;; Topics:	core-functions


;; Write a function which takes a vector of keys and a vector of values and constructs a map from them.

(= (__ [:a :b :c] [1 2 3]) {:a 1, :b 2, :c 3})

(= (__ [1 2 3 4] ["one" "two" "three"]) {1 "one", 2 "two", 3 "three"})

(= (__ [:foo :bar] ["foo" "bar" "baz"]) {:foo "foo", :bar "bar"})

;; Special Restrictions
;; zipmap

#(into {} (map vector %1 %2))


;; Greatest Common DivisorSolutions
;; Difficulty:	Easy
;; Topics:	


;; Given two integers, write a function which returns the greatest common divisor.

(= (__ 2 4) 2)

(= (__ 10 5) 5)

(= (__ 5 7) 1)

(= (__ 1023 858) 33)

(fn [x y]
  (if (zero? y) x (recur y (rem x y))))


;; Set IntersectionSolutions
;; Difficulty:	Easy
;; Topics:	set-theory


;; Write a function which returns the intersection of two sets. The intersection is the sub-set of items that each set has in common.

(= (__ #{0 1 2 3} #{2 3 4 5}) #{2 3})

(= (__ #{0 1 2} #{3 4 5}) #{})

(= (__ #{:a :b :c :d} #{:c :e :a :f :d}) #{:a :c :d})

(fn [s1 s2] (set (filter s1 s2)))

;; Simple closuresSolutions
;; Difficulty:	Easy
;; Topics:	higher-order-functions math


;; Lexical scope and first-class functions are two of the most basic building blocks of a functional language like Clojure. When you combine the two together, you get something very powerful called lexical closures. With these, you can exercise a great deal of control over the lifetime of your local bindings, saving their values for use later, long after the code you're running now has finished.

;; It can be hard to follow in the abstract, so let's build a simple closure. Given a positive integer n, return a function (f x) which computes xn. Observe that the effect of this is to preserve the value of n for use outside the scope in which it is defined.

(= 256 ((__ 2) 16),
       ((__ 8) 2))

(= [1 8 27 64] (map (__ 3) [1 2 3 4]))

(= [1 2 4 8 16] (map #((__ %) 2) [0 1 2 3 4]))

(fn [n]
  #(apply * (repeat n %)))


;; Re-implement IterateSolutions
;; Difficulty:	Easy
;; Topics:	seqs core-functions


;; Given a side-effect free function f and an initial value x write a function which returns an infinite lazy sequence of x, (f x), (f (f x)), (f (f (f x))), etc.

(= (take 5 (__ #(* 2 %) 1)) [1 2 4 8 16])

(= (take 100 (__ inc 0)) (take 100 (range)))

(= (take 9 (__ #(inc (mod % 3)) 1)) (take 9 (cycle [1 2 3])))

;; Special Restrictions
;; iterate

(fn f [g x] (lazy-seq (cons x (f g (g x)))))


;; Cartesian ProductSolutions
;; Difficulty:	Easy
;; Topics:	set-theory


;; Write a function which calculates the Cartesian product of two sets.

(= (__ #{"ace" "king" "queen"} #{"♠" "♥" "♦" "♣"})
   #{["ace"   "♠"] ["ace"   "♥"] ["ace"   "♦"] ["ace"   "♣"]
     ["king"  "♠"] ["king"  "♥"] ["king"  "♦"] ["king"  "♣"]
     ["queen" "♠"] ["queen" "♥"] ["queen" "♦"] ["queen" "♣"]})

(= (__ #{1 2 3} #{4 5})
   #{[1 4] [2 4] [3 4] [1 5] [2 5] [3 5]})

(= 300 (count (__ (into #{} (range 10))
                  (into #{} (range 30)))))


(fn build [x y]
  (set
   (for [x x
         y y]
     [x y])))



;; Group a SequenceSolutions
;; Difficulty:	Easy
;; Topics:	core-functions


;; Given a function f and a sequence s, write a function which returns a map. The keys should be the values of f applied to each item in s. The value at each key should be a vector of corresponding items in the order they appear in s.

(= (__ #(> % 5) [1 3 6 8]) {false [1 3], true [6 8]})

(= (__ #(apply / %) [[1 2] [2 4] [4 6] [3 6]])
   {1/2 [[1 2] [2 4] [3 6]], 2/3 [[4 6]]})

(= (__ count [[1] [1 2] [3] [1 2 3] [2 3]])
   {1 [[1] [3]], 2 [[1 2] [2 3]], 3 [[1 2 3]]})

(fn [f coll] (reduce (fn [r v] (assoc r (f v) (vec (conj (get r (f v)) v)))) {} coll))


;; Infix CalculatorSolutions
;; Difficulty:	Easy
;; Topics:	higher-order-functions math


;; Your friend Joe is always whining about Lisps using the prefix notation for math. Show him how you could easily write a function that does math using the infix notation. Is your favorite language that flexible, Joe? Write a function that accepts a variable length mathematical expression consisting of numbers and the operations +, -, *, and /. Assume a simple calculator that does not do precedence and instead just calculates left to right.

(= 7  (__ 2 + 5))

(= 42 (__ 38 + 48 - 2 / 2))

(= 8  (__ 10 / 2 - 1 * 2))

(= 72 (__ 20 / 2 + 2 + 4 + 8 - 6 - 10 * 9))

(fn [x & args]
  (reduce (fn [r [op n]] (op r n)) x (partition 2 args)))


;; 120Sum of square of digitsSolutions
;; Difficulty:	Easy
;; Topics:	math


;; Write a function which takes a collection of integers as an argument. Return the count of how many elements are smaller than the sum of their squared component digits. For example: 10 is larger than 1 squared plus 0 squared; whereas 15 is smaller than 1 squared plus 5 squared.

(= 8 (__ (range 10)))

(= 19 (__ (range 30)))

(= 50 (__ (range 100)))

(= 50 (__ (range 1000)))

(fn [coll] (let [sums (fn [s]
                           (let [digits (map #(read-string (str %)) (seq (str s)))]
                             (reduce + (map #(* % %) digits))))]
            (count (filter #(< % (sums %)) coll))))
